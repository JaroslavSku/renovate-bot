module.exports = {
  endpoint: 'https://gitlab.com/api/v4/',
  platform: 'gitlab',
  persistRepoData: true,
  logLevel: 'debug',
  onboardingConfig: {
    extends: ['local>groups/subgroups/renovate/renovate-config'],
  },
  autodiscover: true,
};
