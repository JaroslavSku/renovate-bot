import { FlightDTO } from './dto/flight.dto';
import { FlightService } from './flight.service';
import { Body, Controller, Post } from '@nestjs/common';

@Controller('api/v1/flight')
export class FlightController {
  constructor(private readonly flightService: FlightService) {}
  @Post()
  createFlight(@Body() dto: FlightDTO) {
    return this.flightService.create(dto);
  }
}
