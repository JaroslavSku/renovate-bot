import { UserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import { Body, Controller, Post } from '@nestjs/common';

@Controller('api/v1/user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Post()
  createUser(@Body() dto: UserDTO) {
    console.log('test');
    return this.userService.createUser(dto);
  }
}
